<?php
/***********************************************************************************************************************
 * @package     Magento
 * @author      Zahirul Hasan<zhasan@bdipo.com>
 * @copyright   Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

namespace Nascenia\PortWallet\Controller\Index;

/**
 * Class Index
 * @package Nascenia\PortWallet\Controller\Index
 */
class Index extends \Magento\Framework\App\Action\Action
{

    /**
     *
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     *
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     *
     * @var \Magento\Sales\Model\Order
     */
    protected $order;

    /**
     * Index constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Checkout\Model\Session       $checkoutSession
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->messageManager = $context->getMessageManager();

        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $url = $this->getOrder()->getPayment()->getMethodInstance()->getPaymentUrl($this->checkoutSession);
        $this->getResponse()->setRedirect($url);
    }

    /**
     *
     * @return \Magento\Sales\Model\Order
     */
    protected function getOrder()
    {
        if (empty($this->order)) {
            $this->order = $this->checkoutSession->getLastRealOrder();
        }
        return $this->order;
    }
}
