<?php
/***********************************************************************************************************************
 * @package     Magento
 * @author      Zahirul Hasan<zhasan@bdipo.com>
 * @copyright   Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

namespace Nascenia\PortWallet\Controller\Payment;

/**
 * Class Process
 * @package Nascenia\PortWallet\Controller\Payment
 */
class Process extends \Nascenia\PortWallet\Controller\Payment\Ipn
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * Process constructor.
     * @param \Nascenia\PortWallet\Helper\Data      $helper
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Nascenia\PortWallet\Model\ResultJson $resultJson
     * @param \Magento\Sales\Model\OrderFactory     $orderFactory
     * @param \Magento\Checkout\Model\Session       $checkoutSession
     */
    public function __construct(
        \Nascenia\PortWallet\Helper\Data $helper,
        \Magento\Framework\App\Action\Context $context,
        \Nascenia\PortWallet\Model\ResultJson $resultJson,
        \Magento\Sales\Model\Order\PaymentFactory $paymentFactory,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        parent::__construct($helper, $context, $resultJson, $paymentFactory);
        $this->checkoutSession = $checkoutSession;
    }

    public function execute()
    {
        $invoiceNumber = $this->getRequest()->getParam('invoice');
        $amount = $this->getRequest()->getParam('amount');
        $this->getRequest()->getPost()->set('invoice', $invoiceNumber);
        $this->getRequest()->getPost()->set('amount', $amount);
        parent::execute();
        $data = $this->getResultJson()->getData();

        $redirectUrl = "checkout/onepage/success/";

        if (isset($data["error"]) && $data["error"]) {
            $message = isset($data["portWalletMessage"])
                ? __('Your payment is failed due to %1', $data["portWalletMessage"])
                : __('We can\'t process your request right now. Sorry, Payment Failed!!!.');

            $this->helper->getMessageManager()->addErrorMessage($message);
            $this->checkoutSession->setErrorMessage($message);
            $this->checkoutSession->restoreQuote();
            $redirectUrl = "checkout/onepage/failure";
        }
        return $this->_redirect($redirectUrl);
    }
}
