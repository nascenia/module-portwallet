<?php
/***********************************************************************************************************************
 * @package     Magento
 * @author      Zahirul Hasan<zhasan@bdipo.com>
 * @copyright   Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

namespace Nascenia\PortWallet\Controller\Payment;

/**
 * Class Ipn
 *
 * @package Nascenia\PortWallet\Controller\Payment
 */
class Ipn extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Nascenia\PortWallet\Helper\Data
     */
    protected $helper;

    /**
     * @var \Nascenia\PortWallet\Model\ResultJson
     */
    protected $resultJson;

    /**
     * @var \Magento\Sales\Model\Order\PaymentFactory
     */
    protected $paymentFactory;

    /**
     * Ipn constructor.
     *
     * @param \Nascenia\PortWallet\Helper\Data      $helper
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Nascenia\PortWallet\Model\ResultJson $resultJson
     * @param \Magento\Sales\Model\OrderFactory     $orderFactory
     */
    public function __construct(
        \Nascenia\PortWallet\Helper\Data $helper,
        \Magento\Framework\App\Action\Context $context,
        \Nascenia\PortWallet\Model\ResultJson $resultJson,
        \Magento\Sales\Model\Order\PaymentFactory $paymentFactory
    ) {
        $this->helper = $helper;
        $this->resultJson = $resultJson;
        $this->paymentFactory = $paymentFactory;
        parent::__construct($context);
    }

    /**
     *
     * @return \Nascenia\PortWallet\Model\ResultJson
     * @throws \Zend_Json_Exception
     */
    public function execute()
    {
        $className = explode('\\', \get_class($this));
        $className = $className[\sizeof($className) - 2];

        $this->helper->debug("Start Processing Using " . $className);
        $this->helper->debug(
            __(
                "PortWallet Request:\n%1",
                $this->helper::jsonEncode($this->getRequest()->getParams())
            )
        );

        $invoiceNumber = $this->getRequest()->getPost()->get('invoice');
        $amount = $this->getRequest()->getPost()->get('amount');

        if (empty($invoiceNumber)) {
            $this->error("Could Not Process IPN, Invoice number is empty in ipn request");
        } elseif (empty($amount)) {
            $this->error("Could Not Process IPN, Amount is empty in ipn request for invoice: " . $invoiceNumber);
        } else {
            try {
                $this->process($invoiceNumber, $amount);
            } catch (\Exception $exception) {
                $this->error(
                    $exception->getMessage(),
                    null,
                    true,
                    \Magento\Framework\App\Response\Http::STATUS_CODE_500
                );
            }
        }
        $this->helper->debug("Final Response to show:\n%1" . $this->resultJson->getJsonData());
        $this->helper->debug("End Processing Using " . $className);

        return $this->getResultJson();
    }

    /**
     *
     * @param string $invoiceNumber
     * @param string $amount
     *
     * @throws \Exception
     */
    protected function process(string $invoiceNumber, string $amount)
    {
        /**
         * @var \Magento\Sales\Model\Order\Payment $payment
         */
        $payment = $this->paymentFactory->create()->load($invoiceNumber, 'last_trans_id');
        if (!$payment->getId()) {
            $this->error(
                __(
                    "Could Not Process Request for Invoice ID: %2. Invalid invoice ID",
                    $invoiceNumber
                )
            );
        }

        if (!$payment->getAdditionalInformation('transactionPending')) {
            $this->error(
                __(
                    "Could Not Process Request for Order id: %1, Invoice id: %2 already processed",
                    $payment->getOrder()->getId(),
                    $invoiceNumber
                )
            );
        }
        $this->getResultJson()->setStatusHeader(
            \Magento\Framework\App\Response\Http::STATUS_CODE_208
        );
        $this->getResultJson()->setHttpResponseCode(
            \Magento\Framework\App\Response\Http::STATUS_CODE_208
        );

        $this->getResultJson()->setData(
            [
                'error'   => false,
                'message' => "Payment Details Updated"
            ]
        );
        try {
            $payment->authorize(true, (float)$amount);
        } catch (\Exception $exception) {
            $this->error(
                $exception->getMessage(),
                $payment->getAdditionalInformation('portWalletMessage')
            );
        }

        $payment->unsAdditionalInformation('portWalletMessage');
        $payment->save();
        $payment->getOrder()->save();
    }

    /**
     * @return \Nascenia\PortWallet\Model\ResultJson
     */
    public function getResultJson(): \Nascenia\PortWallet\Model\ResultJson
    {
        return $this->resultJson;
    }

    /**
     *
     * @param string|\Magento\Framework\Phrase $message
     * @param string|null $portWalletMessage
     * @param int         $errorCode
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function error(
        $message,
        string $portWalletMessage = null,
        int $errorCode = \Magento\Framework\App\Response\Http::STATUS_CODE_400
    ) {
        $this->helper->error($message, false);
        $this->getResultJson()->setStatusHeader($errorCode);
        $this->getResultJson()->setHttpResponseCode($errorCode);
        $resultData = [
            'error' => true,
            'error_code' => $errorCode,
            'message' => $message
        ];

        if (!empty($portWalletMessage)) {
            $resultData["portWalletMessage"] = $portWalletMessage;
        }
        $this->getResultJson()->setData($resultData);
    }
}
