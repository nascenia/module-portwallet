<?php
/***********************************************************************************************************************
 * @package     Magento
 * @author      Zahirul Hasan<zhasan@bdipo.com>
 * @copyright   Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

namespace Nascenia\PortWallet\Logger;

/**
 * Class Handler
 *
 * @package Nascenia\PortWallet\Logger
 */
class Handler extends \Nascenia\Base\Logger\Handler
{
    protected $logDir = 'var/log/portwallet';
}
