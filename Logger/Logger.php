<?php
/***********************************************************************************************************************
 * @package     Magento
 * @author      Zahirul Hasan<zhasan@bdipo.com>
 * @copyright   Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

namespace Nascenia\PortWallet\Logger;

/**
 * Class Logger
 *
 * @package Nascenia\PortWallet\Logger
 */
class Logger extends \Nascenia\Base\Logger\Logger
{
}
