<?php
/***********************************************************************************************************************
 * @package     Magento
 * @author      Zahirul Hasan<zhasan@bdipo.com>
 * @copyright   Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

namespace Nascenia\PortWallet\Block\System\Config\Form;

/**
 * Class DependableField
 *
 * @package Nascenia\PortWallet\Block\System\Config\Form
 */
class DependableSelect extends \Magento\Config\Block\System\Config\Form\Field
{
    /**
     * @inheritdoc
     */
    protected function _decorateRowHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element, $html)
    {
        $prefix = $element->getContainer()->getGroup('path');
        $dependentGroups =\explode(',', $element->getFieldConfig('dependentGroups'));
        array_walk(
            $dependentGroups,
            function (&$item, $key, $prefix) {
                $item = '#row_' . $prefix . '_' . $item;
            },
            $prefix
        );

        $dependentGroups = \Zend\Json\Json::encode($dependentGroups);

        $html .= '<script type="text/x-magento-init">
            {
                "#' . $element->getHtmlId() . '": {
                    "Nascenia_PortWallet/js/system/config/source/field-watcher": {
                        "dependentGroups":' . $dependentGroups . '
                    }
                }
            }
        </script>';

        return parent::_decorateRowHtml($element, $html);
    }
}
?>

