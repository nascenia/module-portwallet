<?php
/***********************************************************************************************************************
 * @package     Magento
 * @author      Zahirul Hasan<zhasan@bdipo.com>
 * @copyright   Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

namespace Nascenia\PortWallet\Helper;

/**
 * Class Data
 *
 * @package Nascenia\PortWallet\Helper
 */
class Data extends \Nascenia\Base\Helper\AbstractHelper
{
    const CONFIG_PAYMENT_PATH = "payment/portWallet/";

    const CONFIG_PATH_ENABLED= self::CONFIG_PAYMENT_PATH . "enabled";
    const CONFIG_PATH_MODE = self::CONFIG_PAYMENT_PATH . "mode";
    const CONFIG_PATH_API_URL= self::CONFIG_PAYMENT_PATH . "api_url";
    const CONFIG_PATH_PAYMENT_URL= self::CONFIG_PAYMENT_PATH . "payment_url";
    const CONFIG_PATH_API_KEY = self::CONFIG_PAYMENT_PATH . "api_key";
    const CONFIG_PATH_API_SECRET = self::CONFIG_PAYMENT_PATH . "api_secret";
    const CONFIG_PATH_SANDBOX_API_URL = self::CONFIG_PAYMENT_PATH . "sandbox_api_url";
    const CONFIG_PATH_SANDBOX_PAYMENT_URL= self::CONFIG_PAYMENT_PATH . "sandbox_payment_url";
    const CONFIG_PATH_SANDBOX_API_KEY = self::CONFIG_PAYMENT_PATH . "sandbox_api_key";
    const CONFIG_PATH_SANDBOX_API_SECRET = self::CONFIG_PAYMENT_PATH . "sandbox_api_secret";

    /**
     * @var \Nascenia\PortWallet\Model\Portwallet\InvoiceParams
     */
    protected $invoiceParams;

    /**
     * Data constructor.
     * @param \Nascenia\PortWallet\Helper\Context $context
     * @param \Nascenia\PortWallet\Model\ResultJson $resultJson
     */
    public function __construct(
        \Nascenia\PortWallet\Helper\Context $context
    ) {
        parent::__construct($context);
        $this->invoiceParams = $context->getInvoiceParams();
    }

    /**
     *
     * @param int $storeId
     * @return bool
     */
    public function isEnabled(int $storeId = null)
    {
        return (bool)$this->getConfig(self::CONFIG_PATH_ENABLED, $storeId);
    }

    /**
     *
     * @param int $storeId
     * @return bool
     */
    public function isLiveMode(int $storeId = null)
    {
        if ($this->isEnabled($storeId) &&
            $this->getConfig(self::CONFIG_PATH_MODE, $storeId)
            == \Nascenia\PortWallet\Model\Config\Source\Mode::MODE_LIVE) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param int $storeId
     * @return string
     */
    public function getApiUrl(int $storeId = null)
    {
        $apiUrl = "";
        if ($this->isEnabled($storeId)) {
            if ($this->isLiveMode($storeId)) {
                $apiUrl = $this->getConfig(self::CONFIG_PATH_API_URL, $storeId);
            } else {
                $apiUrl = $this->getConfig(self::CONFIG_PATH_SANDBOX_API_URL, $storeId);
            }
        }
        return $apiUrl;
    }

    /**
     *
     * @param int $storeId
     * @return string
     */
    public function getPaymentUrl(int $storeId = null)
    {
        $paymentUrl = "";
        if ($this->isEnabled($storeId)) {
            if ($this->isLiveMode($storeId)) {
                $paymentUrl = $this->getConfig(self::CONFIG_PATH_PAYMENT_URL, $storeId);
            } else {
                $paymentUrl = $this->getConfig(self::CONFIG_PATH_SANDBOX_PAYMENT_URL, $storeId);
            }
        }
        return $paymentUrl;
    }

    /**
     *
     * @param int $storeId
     * @return string
     */
    public function getApiKey(int $storeId = null)
    {
        $apiKey = "";
        if ($this->isEnabled($storeId)) {
            if ($this->isLiveMode($storeId)) {
                $apiKey = $this->getConfig(self::CONFIG_PATH_API_KEY, $storeId);
            } else {
                $apiKey = $this->getConfig(self::CONFIG_PATH_SANDBOX_API_KEY, $storeId);
            }
        }
        return $apiKey;
    }

    /**
     *
     * @param int $storeId
     * @return string
     */
    public function getApiSecret(int $storeId = null)
    {
        $apiSecret = "";
        if ($this->isEnabled($storeId)) {
            if ($this->isLiveMode($storeId)) {
                $apiSecret = $this->getConfig(self::CONFIG_PATH_API_SECRET, $storeId);
            } else {
                $apiSecret = $this->getConfig(self::CONFIG_PATH_SANDBOX_API_SECRET, $storeId);
            }
        }
        return $apiSecret;
    }

    /**
     *
     * @return \Nascenia\PortWallet\Model\Portwallet\InvoiceParams
     */
    public function getInvoiceParams(): \Nascenia\PortWallet\Model\Portwallet\InvoiceParams
    {
        return $this->invoiceParams;
    }

    /**
     *
     * @param string|\Magento\Framework\Phrase $message
     * @param bool $addTrace
     */
    public function debug($message, $addTrace = false)
    {
        if ($message instanceof \Magento\Framework\Phrase) {
            $message = $message->render();
        }
        if ($addTrace) {
            $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 1);
            $message = [
                "message" => $message->render(),
                "trace"   => $trace[0]
            ];
            $message = print_r($message, true);
        }
        $this->getLogger()->addDebug($message);
    }
}
