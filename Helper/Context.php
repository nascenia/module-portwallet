<?php
/***********************************************************************************************************************
 * @package     Magento
 * @author      Zahirul Hasan<zhasan@bdipo.com>
 * @copyright   Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

namespace Nascenia\PortWallet\Helper;

/**
 * Class Context
 *
 * @package Nascenia\PortWallet\Model
 *
 * @method \Nascenia\PortWallet\Model\Portwallet\InvoiceParams getInvoiceParams()
 */
class Context extends \Nascenia\Base\Helper\Context
{
    /**
     * Context constructor.
     * @param \Nascenia\PortWallet\Logger\Logger                   $logger
     * @param \Magento\Framework\UrlInterface                      $urlBuilder
     * @param \Magento\Framework\Message\ManagerInterface          $messageManager
     * @param \Magento\Framework\App\Config\ScopeConfigInterface   $scopeConfig
     * @param \Magento\Framework\App\RequestInterface              $request
     * @param \Magento\Framework\App\ResponseInterface             $response
     * @param \Magento\Store\Model\StoreManagerInterface           $storeManager
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Nascenia\Base\Model\HttpClientFactory               $httpClientFactory
     * @param \Nascenia\PortWallet\Model\Portwallet\InvoiceParams  $invoiceParams
     */
    public function __construct(
        \Nascenia\PortWallet\Logger\Logger $logger,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\App\ResponseInterface $response,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Nascenia\Base\Model\HttpClientFactory $httpClientFactory,
        \Nascenia\PortWallet\Model\Portwallet\InvoiceParams $invoiceParams
    ) {
        parent::__construct(
            $logger,
            $urlBuilder,
            $messageManager,
            $scopeConfig,
            $request,
            $response,
            $storeManager,
            $localeDate,
            $httpClientFactory
        );
        $this->setInvoiceParams($invoiceParams);
    }
}
