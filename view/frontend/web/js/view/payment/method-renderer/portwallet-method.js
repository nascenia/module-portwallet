/***********************************************************************************************************************
 * @package     Magento
 * @author      Zahirul Hasan<zhasan@bdipo.com>
 * @copyright   Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

/* @api */
define([
    'ko',
    'Magento_Checkout/js/view/payment/default'
], function (ko, Component) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Nascenia_PortWallet/payment/portwallet'
        },

        redirectAfterPlaceOrder: false,

        /**
         * Get value of instruction field.
         * @returns {String}
         */
        getInstructions: function () {
            return window.checkoutConfig.payment[this.item.method].instructions;
        },

        afterPlaceOrder: function () {
            window.location.replace(window.checkoutConfig.payment[this.item.method].checkoutUrl);
        }
    });
});
