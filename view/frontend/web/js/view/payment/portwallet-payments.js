/***********************************************************************************************************************
 * @package     Magento
 * @author      Zahirul Hasan<zhasan@bdipo.com>
 * @copyright   Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

/* @api */
define([
    'uiComponent',
    'Magento_Checkout/js/model/payment/renderer-list'
], function (Component, rendererList) {
    'use strict';
    
    rendererList.push(
        {
            type: 'portWalletOnline',
            component: 'Nascenia_PortWallet/js/view/payment/method-renderer/portwallet-method'
        },
        {
            type: 'portWalletMfs',
            component: 'Nascenia_PortWallet/js/view/payment/method-renderer/portwallet-method'
        }
    );

    /** Add view logic here if needed */
    return Component.extend({});
});
