/***********************************************************************************************************************
 * @package     Magento
 * @author      Zahirul Hasan<zhasan@bdipo.com>
 * @copyright   Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

define([
    'jquery',
    'prototype'
], function ($) {
    'use strict';
    return function (config, element) {
        var valueChanged = function () {
            if (parseInt(this.value) === 1) {
                $.each(this.dependentGroups, function (index, elem) {
                    elem.show();
                });
            } else if (parseInt(this.value) === 0) {
                $.each(this.dependentGroups, function (index, elem) {
                    elem.hide();
                });
            }
        };
        $(element).change(valueChanged);
        element.dependentGroups = [];
        $.each(config.dependentGroups, function (index, elem) {
            element.dependentGroups.push($(elem));
        });
        $(function () {
            $(element).change();
        });
    };
});
