<?php
/***********************************************************************************************************************
 * @package     Magento
 * @author      Zahirul Hasan<zhasan@bdipo.com>
 * @copyright   Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

namespace Nascenia\PortWallet\Model\Payment;

/**
 * Class PortWalletOnline
 *
 * @package PortWallet\PortWallet\Model\Payment
 */
class PortWalletOnline extends \Nascenia\PortWallet\Model\Payment\PortWallet
{
    const PAYMENT_METHOD_PORTWALLET_ONLINE_CODE = 'portWalletOnline';

    /**
     * @inheritdoc
     */
    protected $_code = self::PAYMENT_METHOD_PORTWALLET_ONLINE_CODE;

    /**
     * @inheritdoc
     */
    public function isActive($storeId = null)
    {
        if ($this->helper->isEnabled($storeId)) {
            return parent::isActive($storeId);
        }
        return false;
    }
}
