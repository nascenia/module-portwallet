<?php
/***********************************************************************************************************************
 * @package     Magento
 * @author      Zahirul Hasan<zhasan@bdipo.com>
 * @copyright   Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/
namespace Nascenia\PortWallet\Model\Payment;

/**
 * Class ConfigProvider
 * @package Nascenia\PortWallet\Model\Payment
 */
class ConfigProvider implements \Magento\Checkout\Model\ConfigProviderInterface
{
    /**
     * @var string[]
     */
    protected $methodCodes = [
        PortWalletOnline::PAYMENT_METHOD_PORTWALLET_ONLINE_CODE,
        PortWalletMfs::PAYMENT_METHOD_PORTWALLET_MFS_CODE,
    ];

    /**
     * @var \Magento\Payment\Model\Method\AbstractMethod[]
     */
    protected $methods = [];

    /**
     * @var \Magento\Framework\Escaper
     */
    protected $escaper;

    /**
     * ConfigProvider constructor.
     * @param \Magento\Payment\Helper\Data $paymentHelper
     * @param \Magento\Framework\Escaper   $escaper
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function __construct(
        \Magento\Payment\Helper\Data $paymentHelper,
        \Magento\Framework\Escaper $escaper
    ) {
        $this->escaper = $escaper;
        foreach ($this->methodCodes as $code) {
            $this->methods[$code] = $paymentHelper->getMethodInstance($code);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        $config = [];
        foreach ($this->methodCodes as $code) {
            if ($this->methods[$code]->isAvailable()) {
                if (!isset($config['payment'][$code])) {
                    $config['payment'][$code] = [];
                }
                $config['payment'][$code]['instructions'] = $this->getInstructions($code);
                $config['payment'][$code]['checkoutUrl'] = $this->getCheckoutUrl($code);
            }
        }
        return $config;
    }

    /**
     * Get instructions text from config
     *
     * @param string $code
     * @return string
     */
    protected function getInstructions($code)
    {
        return nl2br($this->escaper->escapeHtml($this->methods[$code]->getInstructions()));
    }

    /**
     * Get Payment Url
     *
     * @param string $code
     * @return string
     */
    protected function getCheckoutUrl($code)
    {
        return nl2br($this->escaper->escapeHtml($this->methods[$code]->getCheckoutUrl()));
    }
}
