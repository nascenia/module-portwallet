<?php
/***********************************************************************************************************************
 * @package     Magento
 * @author      Zahirul Hasan<zhasan@bdipo.com>
 * @copyright   Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

namespace Nascenia\PortWallet\Model\Payment;

/**
 * Class PortWallet
 *
 * @package PortWallet\PortWallet\Model\Payment
 */
class PortWallet extends \Magento\Payment\Model\Method\AbstractMethod
{
    const PAYMENT_METHOD_PORTWALLET_CODE = 'portWallet';

    /**
     * @inheritdoc
     */
    protected $_code = self::PAYMENT_METHOD_PORTWALLET_CODE;

    /**
     * Payment Method feature
     *
     * @var bool
     */
    protected $_isGateway = true;

    /**
     * Payment Method feature
     *
     * @var bool
     */
    protected $_canAuthorize = true;

    /**
     * Payment Method feature
     *
     * @var bool
     */
    protected $_canCapture = true;

    /**
     * Payment Method feature
     *
     * @var bool
     */
    protected $_canCaptureOnce = true;

    /**
     * @var \Nascenia\PortWallet\Helper\Data
     */
    protected $helper;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        \Nascenia\PortWallet\Helper\Data $helper,
        array $data = [],
        \Magento\Directory\Helper\Data $directory = null
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data,
            $directory
        );
        $this->helper = $helper;
    }

    /**
     * Get instructions text from config
     *
     * @return string
     */
    public function getInstructions()
    {
        return trim($this->getConfigData('instructions'));
    }

    /**
     * Get Checkout Url
     *
     * @return string
     */
    public function getCheckoutUrl()
    {
        return $this->helper->getUrlBuilder()->getUrl('portWallet');
    }

    /**
     * Generate PortWallet Payment  Url
     *
     * @param \Magento\Sales\Model\Order $order
     * @return string
     */
    public function getPaymentUrl(\Magento\Checkout\Model\Session $checkoutSession)
    {
        $order = $checkoutSession->getLastRealOrder();
        $this->helper->getInvoiceParams()->setOrder($order);
        $httpClient = $this->prepareHttpClient($order->getStoreId());
        $httpClient->setContent($this->helper->getInvoiceParams()->toJson());

        $this->helper->debug(
            __(
                "Attempt To Generate Invoice for order: %.1Request Data:\n%2",
                $order->getIncrementId(),
                $this->helper->getInvoiceParams()->toJson()
            )
        );
        try {
            $httpClient->send();
        } catch (\Exception $ex) {
            throw $ex;
        }

        $content = $httpClient->getResponse()->getContent();

        $this->helper->debug(
            __(
                "Invoice Response for order: %1. Response Data:\n%2",
                $order->getIncrementId(),
                $content
            )
        );

        if ($httpClient->getHeader('Content-Type') == 'application/json') {
            $content = \Zend\Json\Decoder::decode($content);
        }

        $paymentUrl = $this->helper->getUrlBuilder()->getUrl('checkout/onepage/failure');
        if (isset($content->result) && \strtolower($content->result) == 'success') {
            if (isset($content->data->action->url)) {
                $paymentUrl = $content->data->action->url;
            } else {
                $paymentUrl = $this->helper->getPaymentUrl() . '?invoice=' . $content->data->invoice_id;
            }
            $order->getPayment()->setTransactionId($content->data->invoice_id)
                                ->setLastTransId($content->data->invoice_id)
                                ->setAdditionalInformation('transactionPending', true);
            $order->save();
            $paymentUrl = $this->getChildPaymentUrl($paymentUrl);
        } elseif (isset($content->error)) {
            $message= 'Payment Failed: ' . $content->error->explanation;
            $this->helper->getMessageManager()->addErrorMessage($message);
            $checkoutSession->setErrorMessage($message);
            $checkoutSession->restoreQuote();
            $this->helper->error($message, false);
        } elseif (isset($content->message)) {
            $message= 'Payment Failed: ' . $content->message;
            $this->helper->getMessageManager()->addErrorMessage($message);
            $checkoutSession->setErrorMessage($message);
            $checkoutSession->restoreQuote();
            $this->helper->error($message, false);
        } else {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Payment Failed: Unknown Error Occurred')
            );
        }
        return $paymentUrl;
    }

    /**
     * @param int|null $storeId
     * @return \Nascenia\Base\Model\HttpClient
     */
    protected function prepareHttpClient(int $storeId= null)
    {
        $token = $this->helper->getApiSecret($storeId) . \time();
        $token = \base64_encode($this->helper->getApiKey($storeId) . ':' . md5($token));
        $httpClient = $this->helper->getHttpClient();
        $httpClient->setUri($this->helper->getApiUrl() . 'invoice')
                   ->getHeaders()
                   ->addHeaderLine(
                       'Authorization',
                       'Bearer ' . $token
                   );
        return$httpClient;
    }

    /**
     *
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param float                                $amount
     * @return \Magento\Payment\Model\Method\AbstractMethod|void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function authorize(\Magento\Payment\Model\InfoInterface  $payment, $amount)
    {
        parent::authorize($payment, $amount);

        $payment->setTransactionId($payment->getLastTransId());
        /**
         * @var \Magento\Sales\Model\Order\Payment $payment
         */
        $httpClient = $this->prepareHttpClient($this->getStore());
        $path = $httpClient->getUri()->getPath();
        $path .= '/ipn/' . $payment->getTransactionId() . '/' . $amount;
        $httpClient->getUri()->setPath($path);
        $httpClient->setMethod(\Zend\Http\Request::METHOD_GET);
        $this->helper->debug(
            __(
                "Attempt To Get Ipn Validation for order: %1.Invoice Id:\n%2",
                $payment->getTransactionId(),
                $payment->getLastTransId()
            )
        );
        try {
            $httpClient->send();
        } catch (\Exception $ex) {
            throw $ex;
        }

        $content = $httpClient->getResponse()->getContent();

        $this->helper->debug(
            __(
                "Ipn Validation Response for order: %1. Response Data:\n%2",
                $payment->getOrder()->getIncrementId(),
                $content
            )
        );
        if ($httpClient->getHeader('Content-Type') == 'application/json') {
            $content = \Zend\Json\Decoder::decode($content, \Zend\Json\Json::TYPE_ARRAY);
        }
        $error = true;

        if (isset($content['result']) && \strtolower($content['result']) == 'success') {
            if ($content['data']['order']['status'] == 'ACCEPTED') {
                $error = false;
                $payment->setTransactionAdditionalInfo(
                    \Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS,
                    $content
                );
                $this->helper->debug(
                    __(
                        "Creating Transaction for order: %1",
                        $payment->getOrder()->getIncrementId()
                    )
                );

                $payment->setParentTransactionId(null);
                $payment->setIsTransactionApproved(true);
            } else {
                $payment->setIsTransactionApproved(false);
                $message = 'Payment Failed: ' . $content['data']['billing']['gateway']['reason'];
                $payment->setAdditionalInformation('portWalletMessage', $message);
                $payment->setAdditionalInformation('transactionPending', false);
                $payment->getOrder()
                        ->setStatus(\Magento\Sales\Model\Order::STATE_PAYMENT_REVIEW)
                        ->setState(\Magento\Sales\Model\Order::STATE_PAYMENT_REVIEW)
                     ->addStatusHistoryComment($message);
                $payment->setOrderStatePaymentReview(
                    $content['data']['billing']['gateway']['reason'],
                    $payment->getTransactionId()
                );
            }
        } elseif (isset($content['error'])) {
            $message = 'Validation Failed: ' . $content['error']['explanation'];
        } elseif (isset($content['message'])) {
            $message = 'Validation Failed: ' . $content['message'];
        } else {
            $message = 'Payment Failed: Unknown Error Occurred';
        }
        if ($error) {
            $this->helper->error($message);
        }
    }

    /**
     * Get Child Url Part
     *
     * @param string $paymentUrl
     * @return string
     */
    protected function getChildPaymentUrl(string $paymentUrl)
    {
        return $paymentUrl;
    }
}
