<?php
/***********************************************************************************************************************
 * @package     Magento
 * @author      Zahirul Hasan<zhasan@bdipo.com>
 * @copyright   Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

namespace Nascenia\PortWallet\Model\Payment;

/**
 * Class PortWalletMfs
 *
 * @package PortWallet\PortWallet\Model\Payment
 */
class PortWalletMfs extends \Nascenia\PortWallet\Model\Payment\PortWalletOnline
{
    const PAYMENT_METHOD_PORTWALLET_MFS_CODE = 'portWalletMfs';

    /**
     * @inheritdoc
     */
    protected $_code = self::PAYMENT_METHOD_PORTWALLET_MFS_CODE;

    /**
     * @inheritdoc
     */
    protected function getChildPaymentUrl(string $paymentUrl)
    {
        return parent::getChildPaymentUrl($paymentUrl . "&default_network=bkash");
    }
}
