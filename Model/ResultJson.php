<?php
/***********************************************************************************************************************
 * @package     Magento
 * @author      Zahirul Hasan<zhasan@bdipo.com>
 * @copyright   Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

namespace Nascenia\PortWallet\Model;

/**
 * Class ResultJson
 *
 * @package PortWallet\PortWallet\Model
 */
class ResultJson extends \Magento\Framework\Controller\Result\Json
{

    /**
     * @return int
     */
    public function getHttpResponseCode()
    {
        return $this->httpResponseCode;
    }

    /**
     * @return mixed
     * @throws \Zend_Json_Exception
     */
    public function getData()
    {
        return \Zend_Json::decode($this->json);
    }

    /**
     * @return string
     */
    public function getJsonData(): string
    {
        return $this->json;
    }
}
