<?php
/***********************************************************************************************************************
 * @package     Magento
 * @author      Zahirul Hasan<zhasan@bdipo.com>
 * @copyright   Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

namespace Nascenia\PortWallet\Model\Portwallet;

/**
 * Class InvoiceParams
 * @package Nascenia\PortWallet\Model\Payment
 */
class InvoiceParams extends \Magento\Framework\DataObject
{
    /**
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * InvoiceParams constructor.
     *
     * @param \Magento\Framework\UrlInterface $urlBuilder
     */
    public function __construct(\Magento\Framework\UrlInterface $urlBuilder)
    {
        parent::__construct([]);
        $this->urlBuilder =  $urlBuilder;
    }

    public function setOrder(\Magento\Sales\Model\Order $order)
    {
        parent::setOrder(
            [
                 'amount'=> round((float)$order->getGrandTotal(), 2),
                 'currency'=> $order->getOrderCurrency()->getCode(),
                 'redirect_url'=> $this->urlBuilder->getUrl('portWallet/payment/process'),
                 'ipn_url'=> $this->urlBuilder->getUrl('portWallet/payment/ipn'),
                 'reference'=> $order->getIncrementId()
             ]
        );

        $productData = [
             'name'=> [],
             'description'=> [],
         ];
        foreach ($order->getItems() as $orderItem) {
            $productData['name'][] = $orderItem->getName();
            $productData['description'][] = $orderItem->getSku();
        }
        $productData['name'] = \implode(', ', $productData['name']);
        $productData['description'] = \implode(', ', $productData['description']);
        parent::setProduct($productData);

        parent::setBilling($this->getAddressData($order->getBillingAddress()));
        parent::setShipping($this->getAddressData($order->getShippingAddress()));

        return $this;
    }

    /**
     *
     * @param \Magento\Sales\Api\Data\OrderAddressInterface $address
     * @return array
     */
    protected function getAddressData(\Magento\Sales\Api\Data\OrderAddressInterface $address)
    {
        $data = [
            'customer'=>[
                'name' =>$address->getName(),
                'email' =>$address->getEmail(),
                'phone' =>$address->getTelephone(),
                'address' =>[
                    'street' =>\implode(\PHP_EOL, $address->getStreet()),
                    'city' =>$address->getCity(),
                    'state' =>$address->getRegion(),
                    'zipcode' =>$address->getPostcode(),
                    'country' =>$address->getCountryId()
                ]
            ]
        ];
        return $data;
    }
}
