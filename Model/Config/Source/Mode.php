<?php
/***********************************************************************************************************************
 * @package     Magento
 * @author      Zahirul Hasan<zhasan@bdipo.com>
 * @copyright   Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

namespace Nascenia\PortWallet\Model\Config\Source;

/**
 * Class Mode
 *
 * @package Nascenia\PortWallet\Model\Config\Source
 */
class Mode implements \Magento\Framework\Option\ArrayInterface
{
    const MODE_SANDBOX = 0;
    const MODE_LIVE = 1;

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ["value" => self::MODE_LIVE, "label" => __("Live")],
            ["value" => self::MODE_SANDBOX, "label" => __("Sandbox")]
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [self::MODE_LIVE => __("Live"), self::MODE_SANDBOX => __("Sandbox")];
    }
}
